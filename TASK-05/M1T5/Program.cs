﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M1T5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("You will be prompted to input the 24 hour time in two seperate parts - first the hours then the minutes. For example if you wanted 04:23, type 04 (note the 0) in the first prompt and 23 in the second");
            Console.WriteLine(" "); // Break
            Console.WriteLine("Please input the 24h hour time");
            var inputh = Console.ReadLine();
            Console.WriteLine("Please input the 24 hour minute time");
            var inputm = Console.ReadLine();
            Console.WriteLine(" "); //break
            Console.WriteLine("Your 12 hour conversion is:");

            switch (inputh)
                {
                case "00":
                    Console.WriteLine($"12:{inputm}am (Midnight)"); //12am (midnight)
                    break;
                case "01":
                    Console.WriteLine($"1:{inputm}am");
                    break;
                case "02":
                    Console.WriteLine($"2:{inputm}am");
                    break;
                case "03":
                    Console.WriteLine($"3:{inputm}am");
                    break;
                case "04":
                    Console.WriteLine($"4:{inputm}am");
                    break;
                case "05":
                    Console.WriteLine($"5:{inputm}am");
                    break;
                case "06":
                    Console.WriteLine($"6:{inputm}am");
                    break;
                case "07":
                    Console.WriteLine($"7:{inputm}am");
                    break;
                case "08":
                    Console.WriteLine($"8:{inputm}am");
                    break;
                case "09":
                    Console.WriteLine($"9:{inputm}am");
                    break;
                case "10":
                    Console.WriteLine($"10:{inputm}am");
                    break;
                case "11":
                    Console.WriteLine($"11:{inputm}am");
                    break;
                case "12":
                    Console.WriteLine($"12:{inputm}pm (Noon)"); //12pm (noon/midday)
                    break;
                case "13":
                    Console.WriteLine($"1:{inputm}pm");
                    break;
                case "14":
                    Console.WriteLine($"2:{inputm}pm");
                    break;
                case "15":
                    Console.WriteLine($"3:{inputm}pm");
                    break;
                case "16":
                    Console.WriteLine($"4:{inputm}pm");
                    break;
                case "17":
                    Console.WriteLine($"5:{inputm}pm");
                    break;
                case "18":
                    Console.WriteLine($"6:{inputm}pm");
                    break;
                case "19":
                    Console.WriteLine($"7:{inputm}pm");
                    break;
                case "20":
                    Console.WriteLine($"8:{inputm}pm");
                    break;
                case "21":
                    Console.WriteLine($"9:{inputm}pm");
                    break;
                case "22":
                    Console.WriteLine($"10:{inputm}pm");
                    break;
                case "23":
                    Console.WriteLine($"11:{inputm}pm");
                    break;
                case "24":
                    Console.WriteLine($"12:{inputm}pm");
                    break;
                default:
                    Console.WriteLine("you entered an incorrect input, please enter an hour value betwee 00 and 24. Be sure to include the zero in a time like 04:00 (4am)");
                    break;
            }
            Console.WriteLine(" ");
            Console.WriteLine("Press any key to close the program");
            Console.ReadKey();

            
        }
    }
}
