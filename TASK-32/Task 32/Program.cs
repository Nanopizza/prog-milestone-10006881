﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_32
{
    class Program
    {
        static void Main(string[] args)
        {
         
            // Creating the array
            var DaysArray = new string[7]{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

            // Gives days their number and prints to screen
            Console.WriteLine($"{DaysArray[0]} is Day 1");
            Console.WriteLine($"{DaysArray[1]} is Day 2");
            Console.WriteLine($"{DaysArray[2]} is Day 3");
            Console.WriteLine($"{DaysArray[3]} is Day 4");
            Console.WriteLine($"{DaysArray[4]} is Day 5");
            Console.WriteLine($"{DaysArray[5]} is Day 6");
            Console.WriteLine($"{DaysArray[6]} is Day 7");

            // Close program prompt
            Console.WriteLine(" "); //Break
            Console.WriteLine("Press any key to close program");
            Console.ReadKey();

        }
    }
}
