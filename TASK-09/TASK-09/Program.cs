﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_09
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("The leap years for the next 20 years (from 2016) are:");
            Console.WriteLine(" "); //Break
            Console.WriteLine("2016");
            Console.WriteLine("2020");
            Console.WriteLine("2024");
            Console.WriteLine("2028");
            Console.WriteLine("2032");
            Console.WriteLine("2036");

            Console.WriteLine(" "); //Break
            Console.WriteLine("Press any key to close the program");
            Console.ReadKey();


        }
    }
}
