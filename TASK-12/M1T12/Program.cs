﻿using System;

namespace M1T12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please input a number to check if it is even or odd");
            int input;
            input = int.Parse(Console.ReadLine());

            if (input % 2 == 0) // Gets if number is even
            {
                Console.WriteLine("Entered number is an even number");
                Console.WriteLine("Press enter to close the program");
                Console.ReadLine();
            }
            else // if not even then must be odd
            {
                Console.WriteLine("entered number is an odd number");
                Console.WriteLine("Press enter to close the program");
                Console.ReadLine();
            }

        }
    }
}
