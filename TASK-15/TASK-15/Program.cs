﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_15
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> number = new List<int>() { 5, 7, 21, 71, 84 };

            int sum = number.Sum();
            Console.WriteLine("These are the input numbers");
            number.ForEach(Console.WriteLine); //Lists members of list usin foreach loop
            Console.WriteLine(" "); // Break line
            Console.WriteLine($"Added together these numbers = {sum}");

            Console.WriteLine("Press enter to close this program");
            Console.ReadLine(); //So program doesn't close instantly
        }


    }
}

