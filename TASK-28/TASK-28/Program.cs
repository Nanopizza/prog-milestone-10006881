﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;
using System.Threading.Tasks;

namespace TASK_28
{
    class Program
    {
        static void Main(string[] args)
        {
            // Section for user to input words
            Console.WriteLine("Please input the first word you wish to put into the array");
            var word1 = Console.ReadLine();
            Console.WriteLine("Please input the second word you wish to put into the array");
            var word2 = Console.ReadLine();
            Console.WriteLine("Please input the third word you wish to put into the array");
            var word3 = Console.ReadLine();
            
            //Creating the Array
            var createdarray = new string[3] { word1, word2, word3 };

            Console.WriteLine(" "); //Break
            Console.WriteLine("The words you inputted are");

            //Writing contents out on each line using foreach loop
            foreach(var item in createdarray)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine(" "); //Break
            Console.WriteLine("Press any key to close program");
            Console.ReadKey();
       

        }
    }
}
