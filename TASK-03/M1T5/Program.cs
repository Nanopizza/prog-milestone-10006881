﻿using System;

namespace M1T3
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Please Type km2mile or mile2km dependning on which way you wish to convert"); // Asks user what to input | Chose to use a mile2km and km2mile so the user has no confusion as to which command converts which way
            
            var input = (Console.ReadLine());
            const double km2mile = 0.621371; // Equation for when converting KMs to Miles
            const double mile2km = 1.621371; // Equation for when converting Miles to KMs

            switch (input)
            {
                case "km2mile":
                    Console.WriteLine("Enter the KM value you want converted to miles");
                    var userkminput = double.Parse(Console.ReadLine());
                    var km2milesoutput = System.Math.Round(userkminput * km2mile, 4); // rounded to 4 decimal places to ensure output stays reasonable size
                    Console.WriteLine($"{userkminput} KM(s) is {km2milesoutput} Mile(s)");
                    break;
                case "mile2km":
                    Console.WriteLine("Enter the mile value you want converted to KM's");
                    var usermileinput = double.Parse(Console.ReadLine());
                    var miles2kmoutput = System.Math.Round(usermileinput * mile2km, 4); // rounded to 4 decimal places to ensure output remains reasonable size
                    Console.WriteLine($"{usermileinput} Mile(s) is {miles2kmoutput} KM(s)");
                    break;
                default:
                    Console.WriteLine("You used an incorrect input, please run the program again and use the km2mile or mile2km commands");
                    break;
                    
            }
            Console.WriteLine("Please press enter to close the program when ready");
            Console.ReadLine();


        }
    }
}