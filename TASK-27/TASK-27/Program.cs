﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_27
{
    class Program
    {
        static void Main(string[] args)
        {
            var colours = new string[5] { "Red", "Blue", "Yellow", "Green", "Pink" };

            Console.WriteLine("When sorted alphabetically, The colours are:");
            Array.Sort(colours); // sorts alphabetically 
            Console.WriteLine(string.Join(",", colours));

            Console.WriteLine(" "); // Break line
            Console.WriteLine("Press any key to close the program");
            Console.ReadKey(); //Using readkey because it can be used so any key closes program, not just enter.

        }
    }
}
