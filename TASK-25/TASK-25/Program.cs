﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_25
{
    class Program
    {
        static void Main(string[] args)
        {
            int input;
            do
            {
                // loops until it's a number
                Console.WriteLine("Please input a number");
            }
            while (!int.TryParse(Console.ReadLine(), out input));

            Console.WriteLine($"You entered {input}"); 

            Console.WriteLine(" "); //Break
            Console.WriteLine("Press any key to close the program");
            Console.ReadKey();
            }


        }
    }

