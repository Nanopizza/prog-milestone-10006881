﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_39
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please type the month (in full) that you would you like to find the number of Mondays for? (Assuming the first day of the month is a Monday)");
            Console.WriteLine(" "); //Break

            var input = (Console.ReadLine());

            switch (input)
            {
                case "January":
                    Console.WriteLine("There are 5 Mondays in January");
                    break;

                case "February":
                    Console.WriteLine("There are normally 4 Mondays in February, 5 if it's a leap year");
                    break;

                case "March":
                    Console.WriteLine("There are 5 Mondays in March");
                    break;

                case "April":
                    Console.WriteLine("There are 5 Mondays in April");
                    break;

                case "May":
                    Console.WriteLine("There are 5 Mondays in May");
                    break;

                case "June":
                    Console.WriteLine("There are 5 Mondays in June");
                    break;

                case "July":
                    Console.WriteLine("There are 5 Mondays in July");
                    break;

                case "August":
                    Console.WriteLine("There are 5 Mondays in August");
                    break;

                case "September":
                    Console.WriteLine("There are 5 Mondays in September");
                    break;

                case "October":
                    Console.WriteLine("There are 5 Mondays in October");
                    break;

                case "November":
                    Console.WriteLine("There are 5 Mondays in November");
                    break;

                case "December":
                    Console.WriteLine("There are 5 Mondays in December");
                    break;

                default:
                    Console.WriteLine("You entered an incorrect Input, please input January, February, March, April, May, June, July, August, September, October, November or December");
                    break;
            }

            Console.WriteLine(" "); //break
            Console.WriteLine("Press any key to close the program");
            Console.ReadKey();


            }

        }
    }

