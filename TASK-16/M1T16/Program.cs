﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M1T16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please input word to count letters from");
            var input = (Console.ReadLine());

            int lettercount = input.Length;
            Console.WriteLine($"There are {lettercount} letters in your word");

            Console.WriteLine("Press enter to exit the program");
            Console.ReadLine();
        }
    }
}
