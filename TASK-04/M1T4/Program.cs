﻿using System;

namespace M1T4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please type c2f to convert celsius to fahrenheit or f2c to convert fahrenheit to celsius."); //used the c2f and f2c commands to avoid confusion as to which command performs which function

            var input = (Console.ReadLine());
            const double c2f = 1.8; // Equation for when converting Celcius to Fahrenheit
            const double f2c = 0.5556; // Equation for when converting Miles to KMs

            switch (input)
            {
                case "c2f":
                    Console.WriteLine("Enter the celsius (c) value you want converted to fahrenheit (f)");
                    var c2finput = double.Parse(Console.ReadLine());
                    var c2foutput = System.Math.Round(c2finput * c2f + 32, 1); // rounded to 1 decimal place given that temperatures are usually only read to 1 d.p in most cases
                    Console.WriteLine($"{c2finput} celsius (c) is {c2foutput} fahrenheit (f)");
                    break;
                case "f2c":
                    Console.WriteLine("Enter the fahrenheit (f) value you want converted to celsius (c)");
                    var f2cinput = double.Parse(Console.ReadLine());
                    var f2coutput = System.Math.Round((f2cinput-32) * f2c , 1); // rounded to 1 decimal place given that temperatures are usually only read to 1 d.p in most cases
                    Console.WriteLine($"{f2cinput} fahrenheit (f) is {f2coutput} celsius (c)");
                    break;
                default:
                    Console.WriteLine("You used an incorrect input, please run the program again and use the c2f or f2c commands");
                    break;

            }
            Console.WriteLine("Please press enter to close the program when ready");
            Console.ReadLine();


        }
    }
}
