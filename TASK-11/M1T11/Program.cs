﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace M1T11

{
    class program
    {
        static void Main(string[] args)

        {
            int year;
            Console.WriteLine("Please enter the year you wish to see if it's a leapyear or not in 4 digits (eg. 1997)"); //Prompts user which year they wish to input 
            year = int.Parse(Console.ReadLine());

            if (year % 400 == 0)
                Console.WriteLine($"{year} is Leap");


            else if (year % 100 == 0)
                Console.WriteLine($"{year} is not a Leap Year");


            else if (year % 4 == 0)
                Console.WriteLine($"{year} is a Leap Year");


            else
                Console.WriteLine($"{year} is not a Leap Year");

        }

    }
}