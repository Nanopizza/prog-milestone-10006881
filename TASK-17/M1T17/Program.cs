﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M1T17
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> name = new List<string>(); //Creates the list
            name.Add("Shepard");
            name.Add("Liara");
            name.Add("Garrus");

            Console.WriteLine("The people in this list are:");
            name.ForEach(Console.WriteLine); //Lists members of list usin foreach loop
            Console.WriteLine("Press enter to close this program");
            Console.ReadLine(); //So program doesn't close instantly
          
         }


    }
}
