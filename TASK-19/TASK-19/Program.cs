﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_19
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] number = { 34, 45, 21, 44, 67, 88, 86 };

            List<int> evennumbers = new List<int>(); //Put into lists rather than array. Brief said to put them into array in overall brief but then list in the step by step instructions. 
            List<int> oddnumbers = new List<int>();

            // Loop for [0]
            if (number [0] % 2 == 0)
            {
                Console.WriteLine($"{number[0]} is even"); //math for even
                evennumbers.Add (number[0]);
            }
            else
            {
                Console.WriteLine($"{number[0]} is odd"); //math for odd
                oddnumbers.Add(number[0]);
            }
            // Loop for [1]
            if (number[1] % 2 == 0)
            {
                Console.WriteLine($"{number[1]} is even"); //math for even
                evennumbers.Add(number[1]);
            }
            else
            {
                Console.WriteLine($"{number[1]} is odd"); //math for odd
                oddnumbers.Add(number[1]);
            }
            // Loop for [2]
            if (number[2] % 2 == 0)
            {
                Console.WriteLine($"{number[2]} is even"); //math for even
                evennumbers.Add(number[2]);
            }
            else
            {
                Console.WriteLine($"{number[2]} is odd"); //math for odd
                oddnumbers.Add(number[2]);
            }
            // Loop for [3]
            if (number[3] % 2 == 0)
            {
                Console.WriteLine($"{number[3]} is even"); //math for even
                evennumbers.Add(number[3]);
            }
            else
            {
                Console.WriteLine($"{number[2]} is odd"); //math for odd
                oddnumbers.Add(number[2]);
            }
            // Loop for [4]
            if (number[4] % 2 == 0)
            {
                Console.WriteLine($"{number[4]} is even"); //math for even
                evennumbers.Add(number[4]);
            }
            else
            {
                Console.WriteLine($"{number[4]} is odd"); //math for odd
                oddnumbers.Add(number[4]);
            }
            // Loop for [5]
            if (number[5] % 2 == 0)
            {
                Console.WriteLine($"{number[5]} is even"); //math for even
                evennumbers.Add(number[5]);
            }
            else
            {
                Console.WriteLine($"{number[5]} is odd"); //math for odd
                oddnumbers.Add(number[5]);
            }
            // Loop for [6]
            if (number[6] % 2 == 0)
            {
                Console.WriteLine($"{number[6]} is even"); //math for even
                evennumbers.Add(number[6]);
            }
            else
            {
                Console.WriteLine($"{number[6]} is odd"); //math for odd
                oddnumbers.Add(number[6]);
            }


            Console.WriteLine(" "); //Break
            Console.WriteLine("The even numbers in the array are:");
            Console.WriteLine(String.Join(",", evennumbers));

            Console.WriteLine(" "); //Break
            Console.WriteLine("Press any key to close the program");
            Console.ReadKey();
        }
    }
}
