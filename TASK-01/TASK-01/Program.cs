﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is your name?"); // Tells user what they need to input
            var name = Console.ReadLine(); // Gets user input

            Console.WriteLine("How old are you?"); //Asks user for age
            var age = Console.ReadLine(); //Gets user input

            Console.WriteLine(" "); //Break
            Console.WriteLine("Your name is " + name + " and you are " + age + " years old. (Written using String Concatenation)"); // Using String Concatenation
            Console.WriteLine("Your name is {0} and you are {1} years old (Written using positional formatting)", name, age ); // Using Positional Formatting
            Console.WriteLine($"Your name is {name} and you are {age} years old. (Written using string interpolation)"); // Using String Interpolation 

            Console.WriteLine(" "); //Break
            Console.WriteLine("Press any key to close the program");
            Console.ReadKey();
        
        }
    }
}
